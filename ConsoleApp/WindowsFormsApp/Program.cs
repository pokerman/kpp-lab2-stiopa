﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace WindowsMySin
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            double x = double.Parse(txt_x.Text);
            int n = int.Parse(txt_n.Text);
            //вызов метода вычисления sin(x) из библиотеки
            double my_sinus = ClassLibrary1.Class1.Sin(x, n);
            //вызов метода из класса Math
            double sinus = Math.Sin(x);
            txt_y1.Text = my_sinus.ToString();
            txt_y2.Text = sinus.ToString();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}