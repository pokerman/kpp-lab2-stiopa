﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace ConsoleMySin
{
    class Program
    {
        //Консольний варіант обчислення та виводу
        static void Main(string[] args)
        {
            //Введення даних
            Console.WriteLine("Введiть x- кут в радiанах");
            double x = double.Parse(Console.ReadLine());

            Console.WriteLine("Введiть показник степеня n");
            int n = int.Parse(Console.ReadLine());

            //Обчислення з використанням бібліотеки
            double my_arcsin = ClassLibrary1.Class1.Arcsin(x, n);
            double arcsin = Math.Asin(x);
            double delta = arcsin - my_arcsin;

            //Вивід обчислень
            Console.WriteLine("my_arcsin= {0}, arcsin={1}, delta={2}", my_arcsin,
           arcsin, delta);
            Console.ReadKey();
        }
    }
}