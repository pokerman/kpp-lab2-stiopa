﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        //Підключення дизайну об'єкта
        public Form1()
        {
            InitializeComponent();
        }

        //Кнопка виходу
        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //При нажжаті кнопки...
        private void button3_Click(object sender, EventArgs e)
        {
            //Зчитування
            double x = double.Parse(textBox5.Text);
            int n = int.Parse(textBox6.Text);

            //Обчислення
            double my_arcsin = ClassLibrary1.Class1.Arcsin(x, n);

            double arcsin = Math.Asin(x);

            //Вивід обчислень
            textBox7.Text = my_arcsin.ToString();
            textBox8.Text = arcsin.ToString();

        }
    }
}
