﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    ///Обчислення математичних фунцій 
    public class Class1
    {
        //Arсsin з використанням ряду Тейлора
        public static double Arcsin(double x, int n)
        {
            double result = 0;
           for (int i = 1; i <= n; i++)
            {
                result = result + ((2 * i - 1) * Math.Pow(x, 2 * i + 1)) / (2 * i * (2 * i + 1));
            }

            return result + x;
        }
       
    }

}

